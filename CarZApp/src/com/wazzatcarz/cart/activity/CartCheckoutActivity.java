package com.wazzatcarz.cart.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

import com.wazzatcarz.activity.R;
import com.wazzatcarz.common.CVLoadOverListener;
import com.wazzatcarz.common.CVLoadTask;

public class CartCheckoutActivity extends Activity implements CVLoadOverListener{

	CVLoadTask bgThread;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cart_checkout);
		
		bgThread = new CVLoadTask(this, 2);
		bgThread.setCVLoadOverListener(this);
		bgThread.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cart_checkout, menu);
		return true;
	}

	@Override
	public void onDataLoaded(boolean success) {
		// TODO Auto-generated method stub
		if( success == true){
			Toast.makeText(this, "Database updated on local disk.", Toast.LENGTH_SHORT).show();
		}
	}

}
