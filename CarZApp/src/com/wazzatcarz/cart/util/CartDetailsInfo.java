package com.wazzatcarz.cart.util;

import java.util.ArrayList;

public class CartDetailsInfo {

	
	/**
	 * Only instance of the this class
	 */
	private static CartDetailsInfo instance = null;
	/** List of all the items present in the cart
	 * 
	 */
	private static ArrayList<String> GLOBAL_CART = new ArrayList<String>();
	
	
	/**
	 * 
	 * Private constructor to make this class singleton.
	 * Only one instance of this class should be present;
	 */
	private CartDetailsInfo(){
		
	}
	
	
	/**
	 * Static function to get the instance of the class.
	 * 
	 * @return Only instance of the class
	 */
	public static CartDetailsInfo getInstance(){
		if( instance == null){
			instance =  new CartDetailsInfo();
		}
		return instance;
	}


	public static ArrayList<String> getGLOBAL_CART() {
		return GLOBAL_CART;
	}


	public static void setGLOBAL_CART(ArrayList<String> gLOBAL_CART) {
		GLOBAL_CART = gLOBAL_CART;
	}
	
	public static void addToGLOBAL_CART(String newItem){
		GLOBAL_CART.add(newItem);
	}
		
}
