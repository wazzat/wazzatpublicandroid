package com.wazzatcarz.common;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


public class CVLoadTask extends  AsyncTask<Void, Integer, Void>{
	public native void LoadData();
	public native String UpdateData(String name);
	public native void UpdateDataLocal();

	private Context _parentActivity;
	private int flag;
	public String name;

	ArrayList<CVLoadOverListener> listeners = new ArrayList<CVLoadOverListener>();
	
	
	static {
		System.loadLibrary("opencv_java");
		System.loadLibrary("native_sample");
	}
	
	public  CVLoadTask(Context context, int fl) {
		_parentActivity =  context;
		flag = fl;
		Log.i("CVLoadTask","constructor");
	}
	
	private void bgProcess(){
		switch( flag ){
		case 0: LoadData();
				break;
		case 1: name = UpdateData(name);
				break;
		case 2: UpdateDataLocal();
				break;
		default:break;
		}
		
	}
	@Override
   protected void onProgressUpdate(Integer... progress) {
		Log.i("CVLoadTask","Progress Update");

		bgProcess();
    }

	@Override
    protected void onPostExecute(Void arg0) {

		Log.i("CVLoadTask","PostExecute");
//		Intent intent = new Intent(_parentActivity, Start.class);
//		_parentActivity.startActivity(intent);
		if( isCancelled() ) return;
		for( CVLoadOverListener listener:listeners){
			listener.onDataLoaded(true);
		}
		
    }

	@Override
	protected Void doInBackground(Void... arg0) {
		// TODO Auto-generated method stub
		bgProcess();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void setName( String n){
		name = n;
	}
	
	public void setCVLoadOverListener( CVLoadOverListener listener){
		listeners.add(listener);
	}
	
}