package com.wazzatcarz.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wazzatcarz.common.CVLoadOverListener;
import com.wazzatcarz.common.CVLoadTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class CopyOfResultActivity extends Activity implements CVLoadOverListener{

	public static List<String> annotaions_=new ArrayList<String>();
	CVLoadTask bgThread;
	private int selected;
	private Bitmap bmp;
	AlertDialog.Builder builder,builder1; 
	String[] objectsList;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		String res = Sample3View.res + "None";
		annotaions_ = Arrays.asList(res.split(":"));
//		Log.i("hello",annotaions_.get(annotaions_.size() - 1 ) + "last element");
		bmp = Sample3View.bmp;
		BitmapDrawable bdr = new BitmapDrawable(getResources(), bmp);
		LinearLayout result_img = (LinearLayout) findViewById(R.id.result_image_holder);
		double aspectRatio = (double) bmp.getHeight()/bmp.getWidth();
		int targetW = getWindowManager().getDefaultDisplay().getWidth();
		int targetH = (int) (targetW * aspectRatio);
		Bitmap targetBmp = Bitmap.createScaledBitmap(bmp, targetW, targetH, false);
		ImageView queryView = new ImageView(this);
		
		queryView.setImageBitmap(targetBmp);
		result_img.addView(queryView);
		LinearLayout txt_layout = (LinearLayout) findViewById(R.id.result_text_list);
		boolean isEven = false;
		for(int i = 0; i< annotaions_.size() ; i++){
			if( annotaions_.get(i).compareTo("") == 0){
				continue;
			}
			LayoutParams lparams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
			
			final LinearLayout current_ll = new LinearLayout(this);
			current_ll.setOrientation(LinearLayout.HORIZONTAL);
			if(isEven){
				current_ll.setBackgroundColor(Color.DKGRAY);
			}
			else{
				current_ll.setBackgroundColor(Color.BLACK);
			}
			isEven= !isEven;
			current_ll.setLayoutParams(lparams);
			current_ll.setWeightSum(1);
/*			current_ll.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					current_ll.setBackgroundColor(Color.GREEN);
		            //TODO dosomething here make it go somewhere
					updateDataInBackground(selected);
				}
			});
*/
			current_ll.setOnClickListener(new resultsOnClickListener(current_ll, i));
			
			
			ImageView ib = new ImageView(this);
			ib.setLayoutParams(new LayoutParams(50,50));
//			ib.setBackgroundResource(R.drawable.thumnail1);
//			ib.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+
//					"Carz" + File.separator + "thumbs" + File.separator + annotaions_.get(i)));
			ib.setImageBitmap(BitmapFactory.decodeFile("/sdcard/Carz/thumbs/" + annotaions_.get(i) + ".jpg"));
			ib.setMaxHeight(50);
			ib.setMaxWidth(50);
			
			current_ll.addView(ib);
			TextView tv=new TextView(this);
			tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
			tv.setText(annotaions_.get(i));
			tv.setTextColor(Color.WHITE);
			tv.setTextSize(20);
			current_ll.addView(tv);	
			
			txt_layout.addView(current_ll);
			builder = new AlertDialog.Builder(this);
			builder1 = new AlertDialog.Builder(this);
			objectsList= GetObjectsList();
		}
	}

	public void updateDataInBackground(String resultObject){
		bgThread = new CVLoadTask(this,1);
		bgThread.setCVLoadOverListener(this);
		bgThread.setName(resultObject);
		bgThread.execute();
//		saveNewImage(annotaions_.get(sel));
		Log.i("ResultActivity", "updating data with background thread");

	}
	
	private boolean saveNewImage(String fName){

		//Obtains the file path where the taken image will be saved
		File root = new File(Environment.getExternalStorageDirectory(),"Carz/Uploads/");
		if( !root.exists() ){
			root.mkdirs();
		}

		// Create a media file name
		File mediaFile = new File(root, fName+".jpg");
		try {
			FileOutputStream fos = new FileOutputStream(mediaFile);
			bmp.compress(Bitmap.CompressFormat.JPEG, 90, fos);
			fos.flush();
			fos.close();
			Log.d("Main","File overwritten");
			return true;
		}catch (FileNotFoundException e) {
		}catch (IOException e) {
		}
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.result, menu);
		return true;
	}

	@Override
	public void onDataLoaded(boolean success) {
		// TODO Auto-generated method stub
		Log.i("MainActivity","OnDataLoaded");
		if( success == true){
			saveNewImage(bgThread.name);
            Toast.makeText(CopyOfResultActivity.this, "Item marked. Database Updated.", Toast.LENGTH_SHORT).show();		            

		}
	}
	
	public class resultsOnClickListener implements OnClickListener{

		int resultOption;
		String resultObject;
		LinearLayout current_ll;
		public resultsOnClickListener(LinearLayout ll, int n){
			resultOption = n;
			current_ll = ll;
			resultObject = "";
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			current_ll.setBackgroundColor(Color.GREEN);
			if( annotaions_.get(resultOption).compareTo("None") == 0 ){
				Log.e("ResultActivity", "None Clicked1");
				builder1.setTitle("Select the right component from list:");
				builder1.setSingleChoiceItems(objectsList, -1, null );
				builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int whichButton) {
				        	dialog.dismiss();
				        	resultObject = objectsList[((AlertDialog)dialog).getListView().getCheckedItemPosition()];
				        	showSubParts(resultObject);
				        	return;
				        }
				});
				builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int whichButton) {
				        	return;
				        }
				});
				builder1.show();
				Log.e("ResultActivity", "None Clicked");
			}else{
				Log.e("ResultActivity", "object Clicked");
				resultObject = annotaions_.get(resultOption);
			}
			
			if( resultObject.compareTo("")==0 || resultObject.compareTo("None")==0 ){
				return;
			}
			
			showSubParts( resultObject );

//			updateDataInBackground(resultObject);			
		}		
	}
	private void showSubParts(String resultObject){
		Log.i("ResultActivity","Show sub parts " + resultObject);
		String[] objectPartsList = GetObjectPartsList(resultObject);
		final String[] orderOptions=new String[objectPartsList.length+1];
		orderOptions[0] = resultObject;
		final String resObj = resultObject;
		System.arraycopy(objectPartsList, 0, orderOptions, 1, objectPartsList.length);
//		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		if( orderOptions.length != 0){
			Log.i("ResultActivity","received sub parts info"+Integer.toString(objectPartsList.length));
			builder.setTitle("Select the sub-parts for ordering:");
			boolean[] states = new boolean[orderOptions.length];
			builder.setMultiChoiceItems(orderOptions, states, null);
			builder.setPositiveButton("Order", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					SparseBooleanArray checked = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
					String partsToOrder = "";
					for(int i = 0 ; i < checked.size() ; i++ ){
						if( checked.get(i)){
							partsToOrder = orderOptions[i];
						}
					}
					updateDataInBackground(resObj);
					Toast.makeText(CopyOfResultActivity.this, partsToOrder,Toast.LENGTH_SHORT).show();	
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		        	return;
		        }
			});
			builder.show();
		}
	}
	


	
	private native String[] GetObjectsList();
	private native String[] GetObjectPartsList(String objectname);
	
	static{
		System.loadLibrary("native_sample");
	}
}
