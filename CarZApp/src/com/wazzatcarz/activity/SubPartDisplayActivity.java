package com.wazzatcarz.activity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.wazzatcarz.activity.util.ListAssemblySubPart;
import com.wazzatcarz.cart.activity.CartCheckoutActivity;
import com.wazzatcarz.cart.activity.CartViewActivity;
import com.wazzatcarz.cart.util.CartDetailsInfo;

public class SubPartDisplayActivity extends Activity implements View.OnTouchListener{	


	/**
	 * The instance of selected assembly which is provided from previous assembly.
	 */
	public String mSelectedAssembly= "BUMPER_FRONT_ASSY";

	/**
	 * The id by which corresponding mask image is fetched.
	 */
	public String mSelectedAssemblyMask= mSelectedAssembly +"_mask";

	/**
	 * Contains information about the all the mask.
	 */
	public ListAssemblySubPart mSubPartsLists;


	/**
	 * Map of selected part and their corresponding imageview and checkbox
	 */

	public HashMap<String,ImageView> mSelectedImagesView = new HashMap<String, ImageView>();
	public HashMap<String,CheckBox> mSelectedImagesCheckbox = new HashMap<String, CheckBox>();
	

	/**
	 * This list contains all the sub parts for the checkbox
	 */
	private String[] mCheckboxSubPartList ={"Bonnet","Logo","Side-logo","HeadLights","FogLights"};
	LinearLayout holder;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_sub_part_display);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mSelectedAssembly = extras.getString("PartName");
			mCheckboxSubPartList  = extras.getStringArray("SubPartName");
			mSelectedAssemblyMask= mSelectedAssembly +"_mask";
		}
		Log.i("SubPartDisplayActivity",Integer.toString(mCheckboxSubPartList.length));
		loadImageDetailsFromFile();
		drawBackground(0);
		displayCheckbox();
		

//		Button doneButton = (Button)findViewById(R.id.doneButt);
		Button doneButton = new Button(this);
		doneButton.setText("DONE");
		doneButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	private void displayCheckbox() {
		holder = (LinearLayout) findViewById(R.id.list_subparts_ll);
		for( final String cur_item : mCheckboxSubPartList){
			final CheckBox cb =  new CheckBox(this);
			cb.setText(cur_item);
			cb.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
			//cb.setTag(cur_item);
			cb.setTextColor(Color.BLACK);
			cb.setGravity(Gravity.CENTER | Gravity.RIGHT);
		    cb.setChecked(false);
		    cb.setBackgroundColor(Color.WHITE);
		    mSelectedImagesCheckbox.put(cur_item, cb);
		    final float scale = this.getResources().getDisplayMetrics().density;
		    cb.setPadding(cb.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
		            cb.getPaddingTop(),
		            cb.getPaddingRight(),
		            cb.getPaddingBottom());
		    cb.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boolean isChecked = cb.isChecked();
					if( isChecked){
						displayTransparentSubPartMaskOverlay(cur_item);
					}
					else{
						removeSubPartTransparentMaskOverlay(cur_item);						
					}
					
				}
			}); 
		    
		    holder.addView(cb);
		}
	}

	private void loadImageDetailsFromFile() {
		mSubPartsLists = new ListAssemblySubPart();
		mSubPartsLists.load();
	}

	/**
	 * Creates background image. 
	 * 
	 * @param i Uses to identify the path of background image.
	 */
	private void drawBackground(int i) {
		ImageView bg = (ImageView)findViewById(R.id.assembly_image);
		bg.setImageBitmap(BitmapFactory.decodeFile("/sdcard/Carz/assembly/" + mSelectedAssembly + ".jpg"));
		bg.setOnTouchListener(this);

		ImageView bg_mask = (ImageView) findViewById (R.id.sub_parts_rec);
		bg_mask.setImageBitmap(BitmapFactory.decodeFile("/sdcard/Carz/assembly/" + mSelectedAssemblyMask + ".png"));
		
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent ev) {
		boolean readyToLoad = false;

		final int action = ev.getAction();

		final int evX = (int) ev.getX();
		final int evY = (int) ev.getY();

		Log.i("Mask Color" , "NOTHING IS HAPPENING");


		String return_img ="";
		switch (action) {
		case MotionEvent.ACTION_DOWN :	
			Log.i("Mask Color" , "DOWNING THE ACTION");
			Log.i("Mask Color" , "UPPING THE AVTION");
			int touchColor = getMaskColor (evX, evY);		
			Log.i("Mask Color" , Integer.toString(touchColor));
			return_img = mSubPartsLists.getImageName(mSelectedAssembly, touchColor);
			Log.i("Mask Color Return Image" , return_img);
			readyToLoad = true;
			break;

		case MotionEvent.ACTION_UP :
			break;
		default:
			readyToLoad = false;
		} // end switch


		if(readyToLoad){

			if( CartDetailsInfo.getGLOBAL_CART().contains(return_img)){
				removeSubPartTransparentMaskOverlay(return_img);
			}
			else{
				displayTransparentSubPartMaskOverlay(return_img);

				Log.i("Mask Color" , " Loading some shit which I dont know about");
			}
		}
		return readyToLoad;
	}

	private void removeSubPartTransparentMaskOverlay(String return_img) {
		ImageView to_be_removed  = mSelectedImagesView.get(return_img);
		if(to_be_removed != null ){
			FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
			holder.removeView(to_be_removed);
			CartDetailsInfo.getGLOBAL_CART().remove(return_img);
			if( mSelectedImagesCheckbox.get(return_img)!= null ){
				mSelectedImagesCheckbox.get(return_img).setChecked(false);
			}
		}
	}

	private void displayTransparentSubPartMaskOverlay(String return_img) {
		// Overlay image over background
		ImageView transparentbg_mask = new ImageView(this);
		transparentbg_mask.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		String x = return_img.replace(" ", "_");
		transparentbg_mask.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + 
				"/Carz/assembly/" +  x + ".png"));
		CartDetailsInfo.getGLOBAL_CART().add(return_img);

		FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
		holder.addView(transparentbg_mask);
		mSelectedImagesView.put(return_img, transparentbg_mask);
		if( mSelectedImagesCheckbox.get(return_img)!= null ){
			mSelectedImagesCheckbox.get(return_img).setChecked(true);
		}
	}
	
	public int getMaskColor ( int x, int y) {
		ImageView img = (ImageView) findViewById (R.id.sub_parts_rec);		
		img.setDrawingCacheEnabled(true); 
		Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache()); 
		if (hotspots == null) {
			Log.d ("ImageAreasActivity", "Hot spot bitmap was not created");
			return 0;
		} else {
			img.setDrawingCacheEnabled(false);
			return hotspots.getPixel(x, y);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.sub_part_display , menu);	
		
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.checkout:
			Intent i1 = new Intent(SubPartDisplayActivity.this, CartViewActivity.class);
			startActivity(i1);
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
		holder.removeAllViews();
		super.onDestroy();
	}
	
}

