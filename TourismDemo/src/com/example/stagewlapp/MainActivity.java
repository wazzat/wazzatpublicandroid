package com.example.stagewlapp;

import java.util.ArrayList;

import wazzatimagescanner.WLAuthenticate;
import wazzatimagescanner.WLSyncManager;
import wazzatimagescanner.WLSyncStatusListener;
import wazzatimagescanner.camera.WLCameraActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements WLSyncStatusListener {

	ProgressDialog pd ;

	int REQUEST_CODE_CAMERA = 123;

	protected static String RESULT_STRING="Nothing Found";
	protected static ArrayList<String> MULTIPLE_RESULT_STRING=null;
	protected static String[] CHILD_STRING_ARRAY=null;
	protected static String POINT_DIRECTION="NOTHING";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button feedback_btn= (Button) findViewById(R.id.give_feedbackbtn);
		feedback_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i =  new Intent(MainActivity.this,FeedbackActivity.class);
				startActivity(i);
			}
		});
		feedback_btn.setVisibility(View.INVISIBLE);

		Button start_btn= (Button) findViewById(R.id.startProcess);
		start_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i =  new Intent(MainActivity.this,WLCameraActivity.class);
				i.putExtra(WLCameraActivity.USE_DIRECTION, true);

				i.putExtra(WLCameraActivity.USE_GEOLOCATION, true);
				
				i.putExtra(WLCameraActivity.OVERLAY_ID,R.layout.overlay_camera);
				i.putExtra(WLCameraActivity.OVERLAY_BUTTON_ID,R.id.cameraBtn);
				i.putExtra(WLCameraActivity.OVERLAY_CHK_ID,R.id.useSensor);
				i.putExtra(WLCameraActivity.OVERLAY_LR_ID,R.id.liveRes);
				i.putExtra(WLCameraActivity.OVERLAY_DR_ID,R.id.dirRes);

				startActivityForResult(i, REQUEST_CODE_CAMERA);
			}
		});



		Button retry_btn= (Button) findViewById(R.id.retry);
		retry_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i =  new Intent(MainActivity.this,SampleActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		

		if(WLAuthenticate.isUserAuthenticated()){
			retry_btn.setVisibility(View.INVISIBLE);
			start_btn.setVisibility(View.VISIBLE);
		}
		else{
			retry_btn.setVisibility(View.INVISIBLE);
			start_btn.setVisibility(View.VISIBLE);
			//retry_btn.setVisibility(View.VISIBLE);
			//start_btn.setVisibility(View.INVISIBLE);
		}


		final WLSyncManager syncManger = new WLSyncManager(WLSyncManager.SYNC_MODE.UPLOAD_ONLY, this);

		Button down_btn= (Button) findViewById(R.id.sync);
		down_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				syncManger.sync();
			}
		});

		down_btn.setVisibility(View.INVISIBLE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if( requestCode == REQUEST_CODE_CAMERA ){
			if (	resultCode == RESULT_OK && 
					data != null ) {
				
				if( data.hasExtra(WLCameraActivity.SINGLE_RESULT)){ 
					Intent i = new Intent(this, ResultActivity.class);	  
					RESULT_STRING = data.getStringExtra(WLCameraActivity.SINGLE_RESULT);				
					i.putExtra(ResultActivity.RESULT, RESULT_STRING);
					String[] child_obj = null;
					if( data.hasExtra(WLCameraActivity.CHILD)){
						child_obj = data.getStringArrayExtra(WLCameraActivity.CHILD);
						if(child_obj!=null){
							CHILD_STRING_ARRAY = child_obj;
							i.putExtra(ResultActivity.CHILD, CHILD_STRING_ARRAY);
						}
					}				
					startActivity(i);
				}else if(data.hasExtra(WLCameraActivity.MULTIPLE_RESULT)){
					MULTIPLE_RESULT_STRING  = data.getStringArrayListExtra(WLCameraActivity.MULTIPLE_RESULT);
					Intent i = new Intent(this, MultipleResultActivity.class);
					startActivity(i);
				}
			}
			else{
				if(data !=null && !data.hasExtra(WLCameraActivity.NO_RESULT)){
					Intent i = new Intent(this, ResultActivity.class);
					i.putExtra(ResultActivity.RESULT, "No Match Found");
					startActivity(i);	
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			return true;
		}		
		return super.onOptionsItemSelected(item);		
	}

	@Override
	public void onSyncFailure(int error_no) {
		if(pd != null ){
			pd.dismiss();
		}
		Toast.makeText(getApplicationContext(), "Sync Failed!", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onSyncSuccess() {
		if(pd != null ){
			pd.dismiss();
		}
		Toast.makeText(getApplicationContext(), "Sync Done!", Toast.LENGTH_LONG).show();
	}

	@Override
	public void whileSyncing() {

		Toast.makeText(getApplicationContext(), "Syncing.. wait!!", Toast.LENGTH_LONG).show();

	}

	@Override
	public void preSyncTask() {
		Log.i("Main Activity","Show progress Dialog Bar");

		pd = new ProgressDialog(this);
		pd.setTitle("Synchronizaion...");
		pd.setMessage("Please wait....");
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();		
		Log.i("Main Activity","Done");

	}
}