package wazzatimagescanner;

import wazzatimagescanner.Direction.DIRECTION;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class WLCompassManager implements SensorEventListener{

	// device sensor manager
	private SensorManager mSensorManager;
	private Context mContext;
	private static Sensor accelerometer;
	private static Sensor magnetometer;
	private static float[] mAccelerometer = null;
	private static float[] mGeomagnetic = null;

	private int mAzimuthDegree =-1;
	private int mPitchDegree =-1;	// May be used in long long ago in far away galaxy
	private int mRollDegree =- 1;   
	
	private DIRECTION mCurrentDirection = DIRECTION.NOT_ABLE_DETECT;
	
	private static WLCompassManager mInstance = null;
	
	public static WLCompassManager getInstance (){
		if(mInstance == null){
			mInstance = new WLCompassManager(WLClientAcitivityInfo.getContext());
		}
		return mInstance;
	}
	
	private WLCompassManager(Context context){
		mContext = context;
		
		mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		Log.i("Direction", "Registering the services");
		register();
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// onSensorChanged gets called for each sensor so we have to remember the values
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			mAccelerometer = event.values;
		}

		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			mGeomagnetic = event.values;
		}

		if (mAccelerometer != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float remapR[] = new float[9];	
			boolean success = SensorManager.getRotationMatrix(R, null, mAccelerometer, mGeomagnetic);
			
			SensorManager.remapCoordinateSystem(R,
					SensorManager.AXIS_X, 
					SensorManager.AXIS_Z,
					remapR);			
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(remapR, orientation);
				// at this point, orientation contains the azimuth(direction), pitch and roll values.
				double azimuth = 180 * orientation[0] / Math.PI;
				double pitch = 180 * orientation[1] / Math.PI;
				double roll = 180 * orientation[2] / Math.PI;
				mAzimuthDegree= (int) (azimuth);
				mPitchDegree = (int) (pitch);
				mRollDegree = (int) (roll);
				Log.i("MainActivity","Azimuth:"+Integer.toString(mAzimuthDegree)+" Pitch:"+Integer.toString(mPitchDegree)+" Roll:"+Integer.toString(mRollDegree));
				
				mCurrentDirection = findDirection();
				
			}
		}

	}
	
	public void register(){
		mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
		mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void unregister(){
		mSensorManager.unregisterListener(this, accelerometer);
	    mSensorManager.unregisterListener(this, magnetometer);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	public DIRECTION getDirection(){
		return mCurrentDirection;
	}

	private DIRECTION findDirection(){
		DIRECTION direction = DIRECTION.NOT_ABLE_DETECT;
		
		//Log.i("MainActivity","Azimuth:"+Integer.toString(mAzimuthDegree)+" Pitch:"+Integer.toString(mPitchDegree)+" Roll:"+Integer.toString(mRollDegree));
		
		if(mAzimuthDegree >= -160 && mAzimuthDegree < -115) direction = DIRECTION.SOUTH_WEST;
		else if(mAzimuthDegree >= -115 && mAzimuthDegree < -70 ) direction = DIRECTION.WEST;
		else if(mAzimuthDegree >= -70 && mAzimuthDegree < -25) direction = DIRECTION.NORTH_WEST;
		else if(mAzimuthDegree >= -25 && mAzimuthDegree < 20)	direction = DIRECTION.NORTH;
		else if(mAzimuthDegree >= 20 && mAzimuthDegree < 65 )	direction = DIRECTION.NORTH_EAST;
		else if(mAzimuthDegree >= 65 && mAzimuthDegree < 110 )	direction = DIRECTION.EAST;
		else if(mAzimuthDegree >= 110 && mAzimuthDegree < 155)	direction = DIRECTION.SOUTH_EAST;
		else  direction = DIRECTION.SOUTH;

		Log.i("Direction", "Camera is pointing toward " + direction.name());			
		return direction;			
	}
}
