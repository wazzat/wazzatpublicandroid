package wazzatimagescanner;

import org.json.JSONException;
import org.json.JSONObject;


import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * Class is responsible for Authenticate the current application.
 * 
 * User MUST call this class to authenticate using the provided TOKEN 
 * before performing any processing task.
 * 
 * @author wazzatLabs
 *
 */

public class WLAuthenticate {

	/* Error Code
	 * 
	 */
	public static final int NO_ERROR = 0;
	public static final int DOWNLOAD_FAILED = 1;
	public static final int DATA_LOAD_FAILED = 2;
	public static final int TOKEN_EXPIRED = 3;
	
	
	/*
	 * Status
	 */	
	private static final int AUTHENTICATION_NOT_INITIATED = 0;
	private static final int NOT_AUTHENTICATED = 1;
	private static final int PENDING_AUTHENTICATION = 2;
	private static final int AUTHENTICATED = 3;

	private static int mAuthenticationState = AUTHENTICATION_NOT_INITIATED;

	private static final String JSON_AUTHORIZED = "authenticate";	

	private static final String JSON_REGISTER_DATE = "creation_date";	
	
	private static final String JSON_IS_ADMIN = "is_admin";

	private static final String DEFAULT_CREATION_DATE = "2014-01-01 00:00:00";
	
	static WLAuthenticateListener mAuthenticateListener ;
	
	private static boolean mTokenAlreadyCreated = false;;
		
	private WLAuthenticate(){
		/* No need to create its object */
	}

	
	/**
	 * Call this function at the beginning of the application. This function
	 * MUST be called before initiating any processing jobs.
	 * 
	 * SDK can only be used if authenticate is successful.
	 * 
	 * This function spawn a separate thread to authenticate the token.
	 * It is advised to implement the interface WLAuthenticateListener to 
	 * track the progress of the thread.
	 * 
	 * @param token - Token provided by Wazzat Labs
	 */
	public static void Authenticate(final String token , WLAuthenticateListener authenticateListener) {		
		
		mAuthenticateListener = authenticateListener;
		mAuthenticationState = PENDING_AUTHENTICATION;
		final WLSharedPreference sharedPref = WLSharedPreference.getInstance();
		
		WLClientAcitivityInfo.setToken(token);
		WLSharedPreference.getInstance().setTokenID(token);
		if(sharedPref.isFirstStart() || sharedPref.isCurrentSDKExpired() ){									

			new WLServerManager(){

				@Override
				protected void onPostExecute(String result) {
					boolean isAuthenticated = false;
					if(result != null){
						try {
							JSONObject json = new JSONObject(result);
							Log.i("WLauthenticate","Json Response: "+json.toString() );
							if( json.get(JSON_AUTHORIZED).toString().equalsIgnoreCase("true") ){
								isAuthenticated = true;
								String creationDate = json.getString(JSON_REGISTER_DATE);
								Log.i("WLAuthenticate","Token has been created on "+ creationDate);
								mTokenAlreadyCreated = false;
								sharedPref.setRegistrationDate(creationDate);		
								
								String isAdminStr = json.getString(JSON_IS_ADMIN);
								if(isAdminStr.equalsIgnoreCase("true")){
									WLClientAcitivityInfo.setClientAsAdmin();
								}													
								
								WLClientAcitivityInfo.setVersion(0);
								sharedPref.setVersionNumber(0);
							}else{
								sharedPref.setRegistrationDate(DEFAULT_CREATION_DATE);
							}
						} catch (JSONException e) {
							Log.d("Illegal Json Data", e.getLocalizedMessage());				
						}
					}
					
					mAuthenticationState = NOT_AUTHENTICATED;
					if(isAuthenticated){
						mAuthenticationState = AUTHENTICATED;
						if(mAuthenticateListener != null){

							mAuthenticateListener.onAuthenticationSuccess();
						}
					}
					else{			
						if(mAuthenticateListener != null){

							mAuthenticateListener.onAuthenticationFailure(DATA_LOAD_FAILED);
						}
					}						
				}

				@Override
				protected void onProgressUpdate(Integer... values) {
					if(mAuthenticateListener != null){

						mAuthenticateListener.whileAuthenticating();
					}
				}

				@Override
				protected ProgressBar getProgressBar() {
					if(mAuthenticateListener != null){

						return mAuthenticateListener.getProgressBar();
					}
					return null;
					
				}

			}.execute(token);
		}
		else{
			Log.i("WL Authenticate", " TOKEN is already created" );
			mTokenAlreadyCreated = true;

			mAuthenticationState = NOT_AUTHENTICATED;
			if(!sharedPref.isCurrentSDKExpired()){
				Log.i("WL Authenticate", " TOKEN is not expired" );
				
				WLClientAcitivityInfo.setVersion(sharedPref.getVersionNumber());
				
				
				new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						Log.i("DataLoader","Loading data....");
						
						if( WLData.loadData()){
							mAuthenticationState = AUTHENTICATED;
						}

						Log.i("DataLoader","Data load completed");
						
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						if(mAuthenticationState ==  AUTHENTICATED){
							if(mAuthenticateListener != null){
								mAuthenticateListener.onAuthenticationSuccess();
							}
						}
						else{
							if(mAuthenticateListener != null){
								mAuthenticateListener.onAuthenticationFailure(DATA_LOAD_FAILED);
							}
						}
					}

					@Override
					protected void onProgressUpdate(Void... values) {
						if(mAuthenticateListener != null){
							mAuthenticateListener.whileAuthenticating();
						}
					}
				}.execute();
			}
			else{
				Log.i("Wl Authenticate", " TOKEN is expired" );				
				
				if(mAuthenticateListener != null){									
					mAuthenticateListener.onAuthenticationFailure(TOKEN_EXPIRED);
				}
			}		
		}
	}
	
	public static void Authenticate(final String token ){
		Authenticate(token, null);
	}

	/**
	 * Returns if the current Token is authenticated successful.
	 * 
	 * @return true, if the provided token is authenticated successful,otherwise returns false.
	 */
	public static boolean isUserAuthenticated(){
		return mAuthenticationState == AUTHENTICATED;
	}
	
	public static boolean alreadyAuthenticated(){
		return mTokenAlreadyCreated;	
	}
}
