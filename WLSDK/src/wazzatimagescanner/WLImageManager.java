package wazzatimagescanner;

import android.util.Log;

public class WLImageManager {

	private byte[] mLastProcessedImageData = null;
	
	private String mLastProcessedImagePath = null;
	
	private static WLImageManager mInstance = null;
	
	private boolean isImagePathUsed = false; 

	
	private WLImageManager() {
	}

	public static WLImageManager getInstance(){
		if(mInstance == null){
			mInstance = new WLImageManager();
		}
		return mInstance;
	}

	public byte[] getLastProcessedImageData() {
		return mLastProcessedImageData;
	}

	public void setLastProcessedImageData(byte[] lastProcessedImageData) {
		isImagePathUsed =  false;
		Log.i("WLModifier","Storing the bitmap data in the class");

		mLastProcessedImageData = lastProcessedImageData;
	}

	public String getLastProcessedImagePath() {
		return mLastProcessedImagePath;
	}

	public void setLastProcessedImagePath(String mLastProcessedImagePath) {
		isImagePathUsed =  true;
		this.mLastProcessedImagePath = mLastProcessedImagePath;
	}
	
	public boolean isImagePathUsedLast(){
		return isImagePathUsed;
	}
	
}
