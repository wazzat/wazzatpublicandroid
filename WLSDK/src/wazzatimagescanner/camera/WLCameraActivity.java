package wazzatimagescanner.camera;

import java.util.ArrayList;
import java.util.Arrays;

import wazzatimagescanner.WLAuthenticationException;
import wazzatimagescanner.WLCompassManager;
import wazzatimagescanner.WLData;
import wazzatimagescanner.WLScanOverListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WLCameraActivity extends Activity implements WLScanOverListener{

	public static String SINGLE_RESULT ="wazzatimagescanner.camera.RESULT";
	public static String NO_RESULT ="wazzatimagescanner.camera.NO_RESULT";
	public static String CHILD = "wazzatimagescanner.camera.CHILD";
	public static String USE_DIRECTION = "wazzatimagescanner.camera.USE_DIRECTION";
	public static String GET_DIRECTION = "wazzatimagescanner.camera.GET_DIRECTION";	
	public static String USE_GEOLOCATION = "wazzatimagescanner.camera.USE_GEOLOCATION";
	public static String GET_GEOLOCATION = "wazzatimagescanner.camera.GET_GEOLOCATION";
	public static String GET_IMAGE_DATA = "wazzatimagescanner.camera.GET_IMAGE_DATA";
	public static String MULTIPLE_RESULT = "wazzatimagescanner.camera.MULTIPLE_RESULT";
	public static String OVERLAY_ID = "wazzatimagescanner.camera.OVERLAY";
	public static String OVERLAY_BUTTON_ID = "wazzatimagescanner.camera.OVERLAY_BUTTON";
	public static String OVERLAY_CHK_ID = "wazzatimagescanner.camera.OVERLAY_CHK";
	public static String OVERLAY_LR_ID = "wazzatimagescanner.camera.OVERLAY_LR";
	public static String OVERLAY_DR_ID = "wazzatimagescanner.camera.OVERLAY_DR";

	private RelativeLayout mLayout;	
	private WLCameraPreviewRealTime mPreview ;
	boolean captureInitiated = false;

	static boolean isDirectionRequired = false;		
	static boolean isGeoLocationRequired = false;
	
	static TextView mLiveRes = null;
	public static TextView mDirRes = null;
	
	private Context mContext = null;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);				

		mLayout = new RelativeLayout(this);

		isDirectionRequired   = getIntent().getBooleanExtra(USE_DIRECTION, false);
		isGeoLocationRequired   = getIntent().getBooleanExtra(USE_GEOLOCATION, false);
		
		mContext = this;
		// 1. The layout xml which will be overlayed over camera.
		// 2. TODO -Think of an idea to include the overlay camera in the SDK only
		//			No one should be able to change the camera overlay.
		int overlay_id   = getIntent().getIntExtra(OVERLAY_ID, 0);	
		int overlay_btn_id = getIntent().getIntExtra(OVERLAY_BUTTON_ID, 0); // Capture button over camera overlay.
		int overlay_chkbox_id = getIntent().getIntExtra(OVERLAY_CHK_ID, 0); // Capture button over camera overlay.
		int overlay_live_res= getIntent().getIntExtra(OVERLAY_LR_ID, 0); // Capture button over camera overlay.
		int overlay_dir_res= getIntent().getIntExtra(OVERLAY_DR_ID, 0); // Capture button over camera overlay.

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	// We support only landscape orientation.
		
		
		
		WLCompassManager.getInstance(); // To initialize the compass to start the direction calibration

		mLayout.setBackgroundColor(Color.BLACK);
		setContentView(mLayout);

		LayoutInflater controlInflater = LayoutInflater.from(getBaseContext());
		View viewControl = controlInflater.inflate(overlay_id, null);
		LayoutParams layoutParamsControl= new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);

		this.addContentView(viewControl, layoutParamsControl);

		
		ImageButton cameraBtn = (ImageButton) findViewById(overlay_btn_id);
		cameraBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Start the capture event.
				if( !captureInitiated ){
					captureInitiated=true;	    		
					mPreview.initiateCapture();
				}
			}
		});
		cameraBtn.setVisibility(View.INVISIBLE);
		
		mLiveRes = (TextView) findViewById(overlay_live_res);
		mLiveRes.setText("");
		
		mDirRes = (TextView) findViewById(overlay_dir_res);
		mDirRes.setText("");
		
		
		final LocationManager lm = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
		if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			isGeoLocationRequired = false;
			WLData.setGeoLocationUseOff();
		}
						
		CheckBox useSensor = (CheckBox) findViewById(overlay_chkbox_id);
		if(isGeoLocationRequired){
			WLData.setGeoLocationUseOn();
			useSensor.setChecked(true);			
		}
		
		useSensor.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					WLCompassManager.getInstance().unregister();
					isGeoLocationRequired = true;					
					WLData.setGeoLocationUseOn();
					if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
					     Intent gpsOpenIntent = new Intent( 
					                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
					        startActivity(gpsOpenIntent);  
					}
				}
				else{
					WLCompassManager.getInstance().unregister();
					isGeoLocationRequired = false;
					WLData.setGeoLocationUseOff();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		mPreview = new WLCameraPreviewRealTime(this, 0, WLCameraPreviewRealTime.LayoutMode.FitToParent);
		mPreview.setScanOverListener(this);
		LayoutParams previewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mLayout.addView(mPreview, 0, previewLayoutParams);
		WLCompassManager.getInstance().register();
	}	

	@Override
	protected void onPause() {
		super.onPause();
		mPreview.stop();
		mLayout.removeView(mPreview); // This is necessary.
		mPreview = null;		
		WLCompassManager.getInstance().unregister();
	}

	public void onScanDone(boolean success, String result) {
		Intent returnIntent = getIntent();

		if(success){			
			String[] resultList = result.split(":");

			if(resultList.length == 1){
				returnIntent.putExtra(SINGLE_RESULT,result);
			}
			else{
				ArrayList<String> temp = new ArrayList<String>(Arrays.asList(resultList));
				returnIntent.putStringArrayListExtra(MULTIPLE_RESULT, temp);
			}

			Log.i("Java Rsult",result);
			ArrayList<String> childObjects = null;
			try {
				childObjects = WLData.getChildObjects(result);
			} catch (WLAuthenticationException e) {
				
				e.printStackTrace();
			}
			
			if(childObjects != null){
				Log.i("Child","Hurray ! We have children now "+ childObjects.size());
				String[] temp = new String[childObjects.size()];					
				temp = childObjects.toArray(temp);
				returnIntent.putExtra(CHILD, temp);
			}
			
			setResult(RESULT_OK,returnIntent);
		}
		else{		
			setResult(RESULT_CANCELED, returnIntent);					/* Something went bad.. no able to recognize anything*/			
		}
		Log.i("Main","Thing9");		
		finish();				
	}

	@Override
	public void onBackPressed() {	
		super.onBackPressed();
		Intent returnIntent = getIntent();
		returnIntent.putExtra(NO_RESULT, "NULL");
		setResult(RESULT_CANCELED, returnIntent);
		finish();
	}
	
	/*	
	@Override
	public boolean onTouchEvent(MotionEvent event){
	    if(event.getAction() == MotionEvent.ACTION_DOWN) {
	        // Execute your Runnable after 5000 milliseconds = 5 seconds.
	    	if( !captureInitiated ){
	    		captureInitiated=true;	    		
	    		mPreview.initiateCapture();
	    	}
	    }
	    else{

	    }	
		return super.onTouchEvent(event);
	}
	 */

}


