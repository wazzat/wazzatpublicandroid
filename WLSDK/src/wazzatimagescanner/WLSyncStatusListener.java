package wazzatimagescanner;

public interface WLSyncStatusListener {

		
		void whileSyncing();
		
		void onSyncSuccess();
		
		void onSyncFailure(int FailureCode);
		
		void preSyncTask();
		

}
