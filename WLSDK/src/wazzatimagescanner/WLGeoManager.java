package wazzatimagescanner;

import android.content.Context;
import android.location.Location;
import android.util.Log;

public class WLGeoManager {

	private double mLatitude ;
	private double mLongitude ;

	private boolean isLocationRecieved = false;

	private WLGeoLocation mGeoLocation = null;
		
	public WLGeoManager(Context context){
		mGeoLocation = new WLGeoLocation(context);

		mLatitude = -1;
		mLongitude = -1;		
		if(mGeoLocation.canGetLocation()){
			mLatitude = mGeoLocation.getLatitude();
			mLongitude= mGeoLocation.getLongitude();
		}
	}
	
	public boolean isValidLocation(){
		Log.i("GeoManager", "Location: "+ mLatitude + ":" + mLongitude);
		if(mLatitude == -1 || mLongitude == -1){
			return false;
		}
		return true;
	}

	public boolean isLocationSet(){
		return isLocationRecieved;
	}

	public void recalculateLocation(){
		mGeoLocation.findLocation();
		mLatitude=mGeoLocation.getLatitude();
		mLongitude=mGeoLocation.getLongitude();
	}

	public double getLatitude(){
		return mLatitude;

	}
	
	public boolean isLocationSearchOn(){
		return mGeoLocation.canGetLocation();
	}

	public double getLongitude(){
		return mLongitude;				
	}

	public Location getLocation() {
		Location retLoc = new Location("");
		retLoc.setLatitude(mLatitude);
		retLoc.setLongitude(mLongitude);		
		return retLoc;
	}	
}
