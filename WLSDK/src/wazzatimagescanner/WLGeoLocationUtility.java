package wazzatimagescanner;

import wazzatimagescanner.Direction.DIRECTION;
import android.location.Location;
import android.util.Log;

public class WLGeoLocationUtility {
	
	
	public static boolean isDirectionSame = false;
	
	/**
	 * Use to get distance between two geo location in meters . 
	 * 
	 * @param lat1 
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	public static double calculateDistance(double lat1, double lon1,double lat2, double lon2){
		Location l1 = new Location("");
		Location l2 = new Location("");
		double dist=-1;
		l1.setLatitude(lat1);l1.setLongitude(lon1);		
		l2.setLatitude(lat2);l2.setLongitude(lon2);				
		dist = l1.distanceTo(l2);  // in metres
		Log.i("GeoManager","Distance is cool :" + dist);
		return dist;
	}


	private static double calculateBearing(double lat1, double lon1, double lat2, double lon2){
		double longDiff= lon2-lon1;
		double y= Math.sin(longDiff)*Math.cos(lat2);
		double x=Math.cos(lat1)*Math.sin(lat2)-   Math.sin(lat1)*Math.cos(lat2)*Math.cos(longDiff);
		
		return (Math.toDegrees(Math.atan2(y, x))+360)%360;
	}

	public static Direction.DIRECTION getDirectionsBetweenGeoLocations(double srcLat, double srcLon,double destLat, double destLon){
		DIRECTION direction = DIRECTION.EAST;
		Location src = new Location("");
		Location dest = new Location("");

		src.setLatitude(srcLat);
		src.setLongitude(srcLon);

		dest.setLatitude(destLon);
		dest.setLongitude(destLon);
		
		double bearing = calculateBearing(srcLat, srcLon, destLat, destLon);

		Log.i("ERT",(int)bearing+" "+bearing + " "+" " + srcLat+" "+srcLon+" "+destLat+" "+destLon);

		if( (bearing >= 335 && bearing<=360)|| (bearing >= 0 && bearing < 20)) direction = DIRECTION.NORTH;
		else if(bearing >= 20 && bearing < 65 ) direction = DIRECTION.NORTH_EAST;
		else if(bearing >= 65 && bearing < 110) direction = DIRECTION.EAST;
		else if(bearing >= 110 && bearing < 155)	direction = DIRECTION.SOUTH_EAST;
		else if(bearing >= 155 && bearing < 200 )	direction = DIRECTION.SOUTH;
		else if(bearing >= 200 && bearing < 245 )	direction = DIRECTION.SOUTH_WEST;
		else if(bearing >= 245 && bearing < 290)	direction = DIRECTION.WEST;
		else if(bearing >= 290 && bearing < 335)	direction = DIRECTION.NORTH_WEST;
		else direction =  DIRECTION.NOT_ABLE_DETECT;
		Log.i("GeoManager","Direction is " + direction.name());
		return direction;
	}
	
	
	public static boolean isDirectionValidated(){
		return isDirectionSame;
	}
	
	public static void setIfDirectionValidated(boolean isSame){
		isDirectionSame = isSame;
	}

}
