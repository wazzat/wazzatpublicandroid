package wazzatimagescanner;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;


/**
 * 
 * @author wazzatLabs
 *
 */
public class WLClientAcitivityInfo {

	static Context mClientContext ;
	
	static boolean isAdmin  = false;

	static WLAuthenticateListener mClientAcitvity;

	static ConnectivityManager mConnectivityManager ;

	static String mApplicationName;

	static String mToken;
	
	static int mDataVersion;
	
	private static boolean mUseGV = false;

	public static void setContext(Context context){
		mClientContext = context;

		PackageManager lPackageManager = mClientContext.getPackageManager();
		ApplicationInfo lApplicationInfo = null;
		try {
			lApplicationInfo = lPackageManager.getApplicationInfo(mClientContext.getApplicationInfo().packageName, 0);
		} catch (final NameNotFoundException e) {
			mApplicationName="WazzatLabs";
		}
		mApplicationName= (String) (lApplicationInfo != null ? lPackageManager.getApplicationLabel(lApplicationInfo) : "Unknown");
	}

	protected static String getApplicationName(){
		return mApplicationName;
	}
	
	protected static void setClientAsAdmin(){
		isAdmin = true;
	}

	protected static boolean isAdmin(){
		return isAdmin;
	}
	protected static Context getContext(){
		return mClientContext;
	}

	public static void setSystemService(ConnectivityManager system){
		mConnectivityManager = system;
	}

	protected static ConnectivityManager getSystemService (){
		return mConnectivityManager;
	}
	
	protected static String getToken(){
		return mToken;
	}
	protected static void setToken(String token){
		mToken = token;
	}
	
	protected static int getVersion(){
		return mDataVersion;
	}
	protected static void setVersion(int version){
		mDataVersion = version;
	}	
	
	public static void setUseGV(){
		mUseGV = true;		
	}
	
	protected static boolean useGV(){
		return mUseGV;
	}
}
