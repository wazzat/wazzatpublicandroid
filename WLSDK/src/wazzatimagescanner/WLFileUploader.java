package wazzatimagescanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


import android.os.Environment;
import android.util.Log;


public class WLFileUploader{

	static final String WL_URL = "http://www.wazzatlabs.com/uploadFile.php";
	
	static String WL_SERVER_PHP_URL="";

	int maxBufferSize = 1 * 1024 * 1024;
	
	static final ArrayList<String> FILE_LISTS = new ArrayList<String>(){

		private static final long serialVersionUID = 1L;

		{		
			add("Annotations");
			add("DCount");
			add("InvertedIndex");
			add("NumImages");
			add("Objects");
			add("Locations");
			add("ObjectsChild");
		}
	};


	public WLFileUploader(){
		WL_SERVER_PHP_URL = "http://www.wazzatlabs.com/syncserver_2.php?token="+WLClientAcitivityInfo.getToken()+"&mode=UP";
	}
	
	protected boolean upload(){
		boolean ret = true;
		
		/* upload the file */
		for(int i=0 ; i < FILE_LISTS.size() ; ++i){
			String file_name = FILE_LISTS.get(i);
			ret = ret && uploadDataFile(file_name,false);
		}
		
		uploadFiles(); // I don't care about the return value. If it doesn't work, it is still ok.

		/* Execute the server script to process the data*/
		ret = ret && executeServerScript();

	
		
		return ret;
	}

	
	private boolean uploadFiles(){
		boolean ret = true;
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName =  WLClientAcitivityInfo.getApplicationName();		
		String imageDirPath = externalStorage+File.separator+	applicationName + File.separator + "thumbs/";
		File imageDir = new File(imageDirPath);
		if( imageDir.isDirectory() ){
			File[] files = imageDir.listFiles();
			for(int i=0; i < files.length ; i++){
				Log.i("Upload File","File path " + files[i].getAbsolutePath());
				uploadDataFile(files[i].getAbsolutePath(), true);
			}
		}
		return ret;
	}
	
	private boolean  executeServerScript() {
		boolean ret = false;

		URL uri = null;
		HttpURLConnection urlConnection = null;
		try {
			uri = new URL(WL_SERVER_PHP_URL);
			Log.i("Server Sync","server script URL:" + WL_SERVER_PHP_URL);


			urlConnection = (HttpURLConnection) uri.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			String result = "";			

			if(in != null){
				BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
				String line = "";
				while((line = bufferedReader.readLine()) != null)
					result += line;

				in.close();				
			}
			Log.i("Upload Server Sync","Result:" + result);
			if(result.equalsIgnoreCase("OK")){
				ret = true;
			}

		} catch (MalformedURLException e) {
		
			e.printStackTrace();
		}
		catch (IOException e) {
		
			e.printStackTrace();	
		}finally{

			urlConnection.disconnect();
		}
		return ret;		
	}

	private boolean uploadDataFile(String file_name, boolean isImg) {
		boolean ret = false;
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName =  WLClientAcitivityInfo.getApplicationName();
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		File sourceFile = new File(externalStorage+File.separator+applicationName+File.separator+file_name+".txt");
		if(isImg){
			sourceFile = new File(file_name);
		}
		 
		if (!sourceFile.isFile()) {                         
			Log.e("uploadFile", "Source File not exist :"+file_name );              
			return ret;
		}
		try{
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(WL_URL);
			
			String uploadFileName = file_name+"_" +WLClientAcitivityInfo.getToken() +".txt";
			
			if(isImg){
				String imgURL = WL_URL + "?isimg=1&token=" + WLClientAcitivityInfo.getToken();
				url = new URL(imgURL);
				uploadFileName = file_name+"_" + WLClientAcitivityInfo.getToken() ;
			}
			Log.i("Server Sync","File upload URL" + uploadFileName);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection(); 
			conn.setDoInput(true); 
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", uploadFileName); 

			Log.i("Upload_File", "Upload file: " + uploadFileName + " SourceFile: "+ sourceFile);
			
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());         

			dos.writeBytes(twoHyphens + boundary + lineEnd); 
            dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                      + uploadFileName + "\"" + lineEnd);
             
            dos.writeBytes(lineEnd);
			// create a buffer of  maximum size
			bytesAvailable = fileInputStream.available(); 

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

			while (bytesRead > 0) {

				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);   

			}

			// send multipart form data necessary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();
			Log.i("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);

			if(serverResponseCode == 200){
				ret =true;
				InputStream in = new BufferedInputStream(conn.getInputStream());
				String result = "";			

				if(in != null){
					BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
					String line = "";
					while((line = bufferedReader.readLine()) != null)
						result += line;

					in.close();				
				}
				Log.i("Result","Result:"+result);
				Log.i("WLSync","Upload successfull for file : " +file_name);
			}    

			//close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (Exception e) {               
			e.printStackTrace();  
			Log.e("Upload file to server Exception", "Exception : "
					+ e.getMessage(), e);
			ret =false;
		}

		return ret;
	}
	
	
	

}
