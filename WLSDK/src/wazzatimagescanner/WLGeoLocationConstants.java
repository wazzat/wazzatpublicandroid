package wazzatimagescanner;

import android.location.Location;
import android.util.Log;

public class WLGeoLocationConstants {
	private static double mLatitude;
	private static double mLongitude;
	
	
	private static WLGeoLocationConstants mInstance=null;
	
	public static WLGeoLocationConstants getInstance(){
		if(mInstance == null){
			mInstance = new WLGeoLocationConstants();
		}
		return mInstance;
	}

	public double getLatitude() {
		return mLatitude;
	}

	public void setLatitude(double latitude) {
		WLGeoLocationConstants.mLatitude = latitude;
	}

	public double getLongitude() {
		return mLongitude;
	}

	public void setLongitude(double longitude) {
		WLGeoLocationConstants.mLongitude = longitude;
	}

	public boolean isValidLocation(){
		Log.i("GeoManager", "Location: "+ mLatitude + ":" + mLongitude);
		if(mLatitude == -1 || mLongitude == -1){
			return false;
		}
		return true;
	}
	
	public Location getLocation() {
		Location retLoc = new Location("");
		retLoc.setLatitude(mLatitude);
		retLoc.setLongitude(mLongitude);		
		return retLoc;
	}	
}
