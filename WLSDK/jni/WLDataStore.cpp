#include "WLDataStore.h"
#include "WLUtility.h"

using namespace std;

bool WLDataStore::isInstanceCreated = false;
std::string WLDataStore::mExternalStoragepath;
std::string WLDataStore::mPackageName;
std::string WLDataStore::STORAGE_DIR = "/sdcard/Carz/";
WLDataStore* WLDataStore::mInstance = NULL;

WLDataStore::WLDataStore():
performGV(false),
mStatus(DATA_NOT_LOADED)
{
	STORAGE_DIR= mExternalStoragepath+"/"+mPackageName+"/";  /* Data will be stored in the application folder of the packge" */
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress",STORAGE_DIR.c_str());
	
}



WLDataStore::~WLDataStore()
{	
	isInstanceCreated = false;
	delete mInstance ;
	
}

void WLDataStore::initData()
{
	if( loadData() &&
		verifyData() ){
		mStatus = DATA_READY;
	}
	else{
		mStatus = DATA_INVALID;
	}	
}

WLDataStore* WLDataStore::getInstance(string externalStorage, string packgeName)
{
	mExternalStoragepath = externalStorage;
	mPackageName = packgeName;

	if( mInstance == NULL ){
		mInstance = new WLDataStore();
		isInstanceCreated = true;
	}
	return mInstance;
}

bool WLDataStore::loadImageCount()
{
	//=======================================
	//	Loading #images in database
	//=======================================
	bool ret = false;
	ifstream NumImagesFile(getFilePath("NumImages.txt").c_str());
	LOGI("ProgressCheck: NumImages open status: %d", NumImagesFile.is_open());
	if( NumImagesFile.is_open() ){
		NumImagesFile >> N;
		N_orig = N;
		NumImagesFile.close();	
		ret = true;
	}
	return ret;
}

bool WLDataStore::loadObjectList()
{

	bool ret = false;
	//=======================================
	//	Loading distinct objects in database
	//=======================================
	ifstream ObjectsFile(getFilePath("Objects.txt").c_str());
	string tempobj, temppart; 
	int tempcount, lastServerCount;
	
	LOGI("ProgressCheck: Objects open status: %d", ObjectsFile.is_open());
	if( ObjectsFile.is_open() ){
		ObjectsFile >> O;
		for( int i = 0 ; i < O ; i++ ){
			ObjectsFile >> tempcount >> lastServerCount >> tempobj;
			LOGI("ProgressCheck: Objects open status: %d %d %s \n",tempcount,lastServerCount,tempobj.c_str());

			ObjectServerImageCount[tempobj] = lastServerCount;
			Objects[tempobj] = tempcount;
		}
		ObjectsFile.close();
		ret = true;
	}
	return ret;
}
bool WLDataStore::loadInvertedIndex()
{
	//=======================================
	//	Loading Inverted Index File
	//=======================================
	bool ret = false;
	ifstream InvertedIndexFile;
	if( performGV ){
		InvertedIndexFile.open(getFilePath("InvertedIndex_GV.txt").c_str(),ios::in);
	}else{
		InvertedIndexFile.open(getFilePath("InvertedIndex.txt").c_str(),ios::in);
	}

	LOGI("ProgressCheck: InvertedFile open status: %d", InvertedIndexFile.is_open());
	if(  InvertedIndexFile.is_open() ){
		int vword, vcount, vimage, vnum,vx,vy;
		while(InvertedIndexFile.good()){
			InvertedIndexFile >> vword >> vcount;
			for(int i = 0 ; i < vcount ; i++ ){
				InvertedIndexFile >> vimage >> vnum ;
				InvertedFile[vword][vimage] = vnum;
				for ( int j = 0 ; performGV && j < vnum ; j++ ){
					InvertedIndexFile >> vx >> vy;
					KeyPoint ktemp( vx, vy, 1);
					keys[vimage].push_back(ktemp);
					dbWords[vimage].push_back( vword );
				}

			}
		}
		InvertedIndexFile.close();
		__android_log_write(ANDROID_LOG_VERBOSE,"Progress","InvertedIndexFile Loaded");
		LOGI("ProgressCheck: InvertedFile size= %d",(int)InvertedFile.size());
		ret = true;
	}
	return ret;
}

bool WLDataStore::loadHKMean()
{

	//=======================================
	//	Loading the HKMeans Tree
	//=======================================
	bool ret = false;
	ifstream t_file(getFilePath("HKMeans_10000.Tree").c_str(),ios::in);	
	LOGI("ProgressCheck: Tree file open status: %d", t_file.is_open());
	if(  t_file.is_open() ){
		tree = parseTree(t_file,0);
		t_file.close();
		__android_log_write(ANDROID_LOG_VERBOSE,"Progress","HKMeans tree Loaded");
		ret = true;
	}
	return ret;
	
}

bool WLDataStore::loadImageNameAndFrequency()
{
	//======================================================
	//	Loading Image file names and Term Frequency Count
	//======================================================
	bool ret = false;
	string imageListFileName = getFilePath("Annotations.txt");
	string temp, tempLine;
	ifstream imageListFile;
	imageListFile.open(imageListFileName.c_str(),ios::in);
	string TF_filename = getFilePath("DCount.txt");
	int tempTF;
	ifstream TF_file;
	TF_file.open(TF_filename.c_str(), ios::in );

	if( imageListFile.is_open() &&
		TF_file.is_open() ){
	
		for(int i = 1 ; i <= N ; i++ ){
			imageListFile >> temp;
			getline( imageListFile, tempLine);
			ImageList.push_back( temp );
			Annotations.push_back( tempLine );
			TF_file >> tempTF;
			TF.push_back(tempTF);
		}
		imageListFile.close();
		TF_file.close();
		ret = true;
	}
	return ret;
}

bool WLDataStore::loadLocations()
{
	bool ret = false;
	//=======================================
	//	Loading Geo Location of objects
	//=======================================
	ifstream locationFile(getFilePath("Locations.txt").c_str());
	int countLocation = 0;
	string tempobj;
	LOGI("ProgressCheck: LocationsFile open status: %d", locationFile.is_open());
	if( locationFile.is_open() ){
		locationFile >> countLocation;
		for( int i = 0 ; i < countLocation ; i++ ){
			Location temploc;
			locationFile >> tempobj >> temploc.lat >> temploc.lon;
			LOGI("ProgressCheck: Location open status: %0.6f %0.6f %s \n",temploc.lat,temploc.lon,tempobj.c_str());
			LocationList[tempobj] = temploc;
		}
		locationFile.close();
		ret = true;
	}
	return ret;
}

bool WLDataStore::loadChildObjects()
{
	bool ret = false;
	//=======================================
	//	Loading Child of objects
	//=======================================
	ifstream objectsChildFile(getFilePath("ObjectsChild.txt").c_str());
	string tempobj;
	int count;
	LOGI("ProgressCheck: objectsChildFile open status: %d", objectsChildFile.is_open());
	if( objectsChildFile.is_open() ){
		while( objectsChildFile.good() ){
			string temp;
			objectsChildFile >> tempobj >> count;
			
			LOGI("ProgressCheck: ObjectsChildFile reading %s %d",tempobj.c_str(), count);
			vector<string> tempvec;
			while(count>0){
				objectsChildFile >> temp;
				LOGI("ProgressCheck: ObjectsChildFile reading %s %d",temp.c_str(),count);				
				tempvec.push_back(temp);				
				count --;
			}
			objectsChildList.insert( pair<string, vector<string> > (tempobj, tempvec ) );
		}
		objectsChildFile.close();
		ret = true;
		LOGI("ProgressCheck: ObjectsChildFile read done");

	}
	return ret;
}

bool WLDataStore::loadData()
{
	mStatus = DATA_LOADING;
	bool ret = false;
	ret = loadImageCount() &&		
	loadObjectList() &&		
	loadInvertedIndex() &&		
	loadHKMean() &&		
	loadImageNameAndFrequency() &&
	loadLocations()&&
	loadChildObjects();
	
	mStatus = DATA_NOT_LOADED ;	
	if( ret	){
		mStatus = DATA_LOADED;
	}
	return ret;
}

bool WLDataStore::verifyData()
{
	/* This place is reserved if any further checks on data is needed */	
	return true;
}

bool WLDataStore::persistDataLocally()
{
	bool ret = true;
	LOGI("Saving the data back to files");
	string temp_file_name =  getFilePath("InvertedIndex.txt");	
	
	ofstream InvertedIndexFile(temp_file_name.c_str(),ios::out);
	LOGI("ProgressCheck: InvertedIndexfile status: %d", InvertedIndexFile.is_open());
	if( InvertedIndexFile.is_open() ){
		for( map<int, map<int, int > >::iterator it = InvertedFile.begin() ; it != InvertedFile.end() ; ++it ){
			InvertedIndexFile << it->first << " " << (it->second).size() << endl;
			for( map<int, int>::iterator jt = InvertedFile[it->first].begin() ; jt!= InvertedFile[it->first].end() ; ++jt ){
				InvertedIndexFile << jt->first << " " << jt->second << endl;
			}
		}
		
		if(InvertedIndexFile.bad()){
			ret = ret && false;
		}
		
		InvertedIndexFile.close();
		LOGI("Inverted Index Updated");
	}
	else{
		ret = ret && false;
	}
		
	temp_file_name =  getFilePath("Annotations.txt");	
	ofstream AnnotationsFile(temp_file_name.c_str(),ios::out | ios::app);
	
	
	temp_file_name =  getFilePath("DCount.txt");		
	ofstream DCountFile(temp_file_name.c_str(),ios::out | ios::app);
	
	LOGI("ProgressCheck: AnnotationsFile status: %d", AnnotationsFile.is_open());
	LOGI("ProgressCheck: DCountFile status: %d", DCountFile.is_open());
	
	if( AnnotationsFile.is_open() && DCountFile.is_open()){
		for( int i = N_orig ; i < N ; i++ ){
			AnnotationsFile <<  ImageList[i] << "\t" << Annotations[i]<< endl;
			DCountFile << TF[i] << endl;
		}
		AnnotationsFile.close();
		DCountFile.close();
	}
	else{
			ret = ret && false;
	}
	

	temp_file_name =  getFilePath("NumImages.txt");	
	
	ofstream NumImagesFile(temp_file_name.c_str(),ios::out);
	if( NumImagesFile.is_open() ){
		NumImagesFile << N << endl;
		N_orig = N;
		NumImagesFile.close();
	}
	else{
		ret = ret && false;
	}

	//=======================================
	//	Loading distinct objects in database
	//=======================================
	temp_file_name =  getFilePath("Objects.txt");	

	ofstream ObjectsFile(temp_file_name.c_str(),ios::out);
	
	if( ObjectsFile.is_open () ){		
		string tempobj; 
		int tempcount;
		LOGI("ProgressCheck: Objects open status: %d", ObjectsFile.is_open());
		ObjectsFile << O << endl;
		for( map<string, int>::iterator it = Objects.begin(); it != Objects.end() ; ++it ){
			ObjectsFile << it->second << " " <<  ObjectServerImageCount[it->first] << " "<< it->first << endl;		
		}
		ObjectsFile.close();
	}
	else{
		ret = ret && false;
	}
	
	temp_file_name =  getFilePath("Locations.txt");	

	ofstream LocationFile(temp_file_name.c_str(),ios::out);
	
	if( LocationFile.is_open () ){		
		string tempobj; 
		Location temploc;
		LOGI("ProgressCheck: Locations open status: %d", LocationFile.is_open());
		LocationFile << LocationList.size() << endl;
		for( map<string, Location>::iterator it = LocationList.begin(); it != LocationList.end() ; ++it ){
			LocationFile << it->first << " " <<  (it->second).lat << " "<< (it->second).lon << endl;		
		}
		LocationFile.close();
	}
	else{
		ret = ret && false;
	}
	
	temp_file_name =  getFilePath("ObjectsChild.txt");
	ofstream ObjectsChildFile(temp_file_name.c_str(),ios::out);
	
	if( ObjectsChildFile.is_open () ){		
		LOGI("ProgressCheck: ObjectsChildFile open status: %d", ObjectsChildFile.is_open());
		ObjectsChildFile << objectsChildList.size() << endl;
		for( map<string, vector<string > >::iterator it = objectsChildList.begin(); 
		
			it != objectsChildList.end() ; ++it ){
			ObjectsChildFile << it->first << " " << (it->second).size() ;
			
			for(vector<string>::iterator itv = (it->second).begin() ; itv != (it->second).end() ; ++itv){
				ObjectsChildFile << " " << *itv ;
			}			
			ObjectsChildFile <<endl;
		}
		ObjectsChildFile.close();
	}
	else{
		ret = ret && false;
	}
	
	if( ret ) {
		LOGI("Data has been saved");
		return ret;
	}
	LOGI("Data can not be saved");
	return ret;
}

string WLDataStore::getFilePath(std::string file_name){
	string path =  STORAGE_DIR + file_name ;
	LOGI("ProgressCheck: Filename get recieved: %s", path.c_str());
	return path;
}

bool WLDataStore::isDataReady(){	
	return mStatus == DATA_READY;
}
