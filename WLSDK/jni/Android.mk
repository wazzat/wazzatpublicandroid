LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_CAMERA_MODULES:=off

#include ../includeOpenCV.mk
ifeq ("$(wildcard $(OPENCV_MK_PATH))","")
	#try to load OpenCV.mk from default install location
#	include $(TOOLCHAIN_PREBUILT_ROOT)/user/share/OpenCV/OpenCV.mk
  #include  /cygdrive/c/Android/OpenCV-2.4.9-android-sdk/sdk/native/jni/OpenCV.mk
#  include /cygdrive/c/Android/OpenCV-2.4.0-android-bin/OpenCV-2.4.0/share/OpenCV/OpenCV.mk
#	include /home/jay/OpenCV-2.4.0-android-bin/OpenCV-2.4.0/share/OpenCV/OpenCV.mk
	include /home/rvk/OpenCV-2.4.0-android-bin/OpenCV-2.4.0/share/OpenCV/OpenCV.mk
else
  include $(OPENCV_MK_PATH)
endif

LOCAL_MODULE    := wlimagescanner
LOCAL_SRC_FILES := WLBase.cpp WLDataStore.cpp WLNative.cpp
#LOCAL_SHARE_LIBRARIES := /cygdrive/c/Android/OpenCV-2.4.0-android-bin/OpenCV-2.4.0/libs/armeabi/libopencv_java.so
LOCAL_SHARE_LIBRARIES := /home/jay/OpenCV-2.4.0-android-bin/OpenCV-2.4.0/libs/armeabi/libopencv_java.so
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
