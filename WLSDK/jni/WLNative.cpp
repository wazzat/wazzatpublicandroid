#include <jni.h>
#include <vector>

#include <stdio.h>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <fstream>
#include <android/log.h>
#include <sstream>
#include "WLBase.h"

static WLBase mProcessor;

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))

extern "C" {
	JNIEXPORT jboolean  JNICALL Java_wazzatimagescanner_WLNative_LoadData(JNIEnv* env, jobject thiz, jboolean useGV)
	{
		return mProcessor.init(useGV);
	}
}

extern "C" {
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_UpdatePackageInfo(JNIEnv* env, jobject thiz,jstring external_storage, jstring package_info)
	{
		const char *externalStorage_c = env->GetStringUTFChars( external_storage, 0);
		const char *packageInfo_c = env->GetStringUTFChars( package_info, 0);
		std::stringstream ss1;
		ss1 << externalStorage_c;
		std::string externalStorage = ss1.str();
		std::stringstream ss2;
		ss2 << packageInfo_c;
		std::string packageInfo = ss2.str();
		mProcessor.updatePackageInfo(externalStorage,packageInfo);
	}
}

extern "C" {
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_Search(JNIEnv* env, jobject thiz, jint width, jint height,jbyteArray yuv, jintArray bgra,jintArray status,jdouble latitude, jdouble longitude)
	{
		jbyte* _yuv  = env->GetByteArrayElements(yuv, 0);
		jint*  _bgra = env->GetIntArrayElements(bgra, 0);
				
		string result = mProcessor.search(env, thiz,width, height, _yuv,_bgra,latitude,longitude);
		
		env->ReleaseIntArrayElements(bgra, _bgra, 0);
		env->ReleaseByteArrayElements(yuv, _yuv, 0);
		__android_log_print( ANDROID_LOG_DEBUG,"AnnotationFinal!@","%s",result.c_str() );
		return env->NewStringUTF(result.c_str());
	}
}

extern "C" {
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_SearchPath(JNIEnv* env, jobject thiz,jstring imageFilePath, jintArray status)
	{
		/*  TODO */
		string result = "NULL";
		return env->NewStringUTF(result.c_str());
	}
}


extern "C" {
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_FindFeatures(JNIEnv* env, jobject thiz, jint width, jint 
	height, jbyteArray yuv, jintArray bgra)
	{
		jbyte* _yuv  = env->GetByteArrayElements(yuv, 0);
		jint*  _bgra = env->GetIntArrayElements(bgra, 0);
		
		mProcessor.findFeatures(width, height, _yuv, _bgra);
		
		env->ReleaseIntArrayElements(bgra, _bgra, 0);
		env->ReleaseByteArrayElements(yuv, _yuv, 0);	
	}
}

extern "C" {
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_UpdateData(JNIEnv* env, jobject thiz, jstring name)
	{
		const char *imgName = env->GetStringUTFChars( name, 0);
		LOGI("Original Name %s",imgName );
		const char* imgNamePadded  = mProcessor.updateObjectList(imgName);
		LOGI("Padded Name %s", imgNamePadded );
		//char * finalName = imgNamePadded;
		//env->ReleaseStringUTFChars( name, finalName);
		//return env->NewStringUTF(imgNamePadded);
	}
}

extern "C" {
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_UpdateDataLocal(JNIEnv* env, jobject thiz)
	{
		mProcessor.syncObjectListLocally();
	}
}

extern "C" {
	JNIEXPORT jobjectArray JNICALL Java_wazzatimagescanner_WLNative_GetObjectsList(JNIEnv* env, jobject thiz)
	{
		LOGI("NativeActivity","to return object list");
		map<string, int > Objects = mProcessor.getObjectList();
		jobjectArray ret = env->NewObjectArray(Objects.size(), env->FindClass("java/lang/String"),0);
		LOGI("NativeActivity","giving array of objects as strings");
		int i=0;
		for( map<string, int>::iterator it = Objects.begin(); it != Objects.end() ; ++it, i++ ){
			env->SetObjectArrayElement(ret, i, env->NewStringUTF((it->first).c_str()) );
		}
		return ret;
	}
}

extern "C" {
	JNIEXPORT jobjectArray JNICALL Java_wazzatimagescanner_WLNative_GetObjectsChildList(JNIEnv* env, jobject thiz,jstring name)
	{
		LOGI("to return child object list");
		const char *imgName = env->GetStringUTFChars( name, 0);
		string name_str = imgName;
		LOGI("1");
		vector<string> childObjects = mProcessor.getChildObjectList(name_str);
		LOGI("2");
		jobjectArray ret = env->NewObjectArray(childObjects.size(), env->FindClass("java/lang/String"),0);
		LOGI("3");
		LOGI("giving array of child objects as strings");
		int i=0;
		for( vector<string>::iterator it = childObjects.begin(); it != childObjects.end() ; ++it, i++ ){
			LOGI("giving array of child objects as strings %s", (*it).c_str());
			env->SetObjectArrayElement(ret, i, env->NewStringUTF((*it).c_str()) );
		}
		return ret;
	}
}


extern "C" {
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_getSharedPrefAppKey(JNIEnv* env, jobject thiz)
	{
		string application_key = "WLSHARED_PREF";
		return env->NewStringUTF(application_key.c_str());
	}	
}
extern "C"{
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_getSharedPrefTokenExpKey(JNIEnv* env, jobject thiz)
	{
		string token_expiration_key = "TOKEN_EXPIRATION";
		return env->NewStringUTF(token_expiration_key.c_str());
	}
}

extern "C"{
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_getSharedPrefVersionKey(JNIEnv* env, jobject thiz)
	{
		string token_version_key = "TOKEN_DATA_VERSION";
		return env->NewStringUTF(token_version_key.c_str());
	}
}

extern "C"{
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_getSharedPrefScanCountKey(JNIEnv* env, jobject thiz)
	{
		string token_version_key = "API_CALL_COUNT";
		return env->NewStringUTF(token_version_key.c_str());
	}
}

extern "C"{
	JNIEXPORT jstring JNICALL Java_wazzatimagescanner_WLNative_getSharedPrefTokenIDKey(JNIEnv* env, jobject thiz)
	{
		string token_version_key = "TOKEN_ID";
		return env->NewStringUTF(token_version_key.c_str());
	}
}

extern "C"{
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_setGeoLocationUseOn(JNIEnv* env, jobject thiz)
	{
		LOGI("Switching on Geo sensor in Native");
		mProcessor.setGeoLocationUseOn();
		LOGI("Returning back from native");
	}
}
extern "C"{
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_setGeoLocationUseOff(JNIEnv* env, jobject thiz)
	{
		LOGI("Switching off Geo sensor in Native");
		mProcessor.setGeoLocationUseOff();
	}
}
extern "C"{
	JNIEXPORT void JNICALL Java_wazzatimagescanner_WLNative_toggleUseGV(JNIEnv* env, jobject thiz)
	{
		LOGI("Switching off Geo sensor in Native");
		mProcessor.toggleUseGV();
	}
}
