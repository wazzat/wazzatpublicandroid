#ifndef __WL_DATA_STORE_H__
#define __WL_DATA_STORE_H__

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <map>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "WLStruct.h"

using namespace std;
using namespace cv;

enum data_status{	
	DATA_NOT_LOADED = 0,
	DATA_LOADING = 1,
	DATA_LOADED = 2,	
	DATA_READY = 3,
	DATA_INVALID = 4
};

class WLDataStore 
{
	public:
	
		static WLDataStore* getInstance(string externalStorage, string packgeName);
		~WLDataStore();
		bool isDataLoaded(){
			return mStatus == DATA_READY;
		}
		void init();
		
		void initData();
		
		bool isDataReady();
		
		bool persistDataLocally();		
		
		int N ;
		int N_orig;
		map< int, map< int, int > > InvertedFile;
		Tree *tree;
		vector < string > ImageList;
		vector < string > Annotations;
		vector < int > TF;
		map< int, int > QueryHist;
		vector < int > QWords;
		map< int, vector<KeyPoint> > keys;
		map<int, vector<int> > dbWords;
		int O;
		map< string, int > ObjectServerImageCount;
		map< string, int > Objects;		
		map< string, Location > LocationList;
		map<string, vector<string> > objectsChildList;
		bool performGV;

		
	private:
		
		WLDataStore();   /* Making this class singleton to ensure only one copy of data exists at a time. */
		
		bool loadData();
		
		bool loadImageCount();
		bool loadObjectList();
		bool loadInvertedIndex();
		bool loadHKMean();
		bool loadImageNameAndFrequency();
		bool loadLocations();
		bool loadChildObjects();
		bool verifyData();
		std::string getFilePath(std::string file_name);

		static bool isInstanceCreated ;
		static WLDataStore *mInstance ;
		data_status mStatus;
		static std::string mExternalStoragepath;
		static std::string mPackageName;
		static std::string  STORAGE_DIR;
		
	
};

#endif //header guard ends here
